from django.test import TestCase, Client
from .views import Counter

# Create your tests here.

class CounterTest(TestCase):
    def test_creation(self):
        """Creation works"""
        counter = Counter()
        self.assertEqual(counter.count, 0)

    def test_increment(self):
        """Increment works"""
        counter = Counter()
        inc = counter.increment()
        self.assertEqual(inc, 1)
        inc = counter.increment()
        self.assertEqual(inc, 2)
        inc = counter.increment()
        self.assertEqual(inc, 3)

class Cmstest(TestCase):
    """Hacer un test por cada url que haya en el urls.py"""
    def test_index(self):
        """Check index page works"""
        c = Client()
        response = c.get('/cms/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>CMS Django</h1>', content)

    def test_not_found(self):
        """Check 404 page works"""
        c = Client()
        response = c.get('/cms/not_found/')
        self.assertEqual(response.status_code, 404)
        content = response.content.decode('utf-8')
        self.assertIn('Not Found', content)

    def test_update_create_content(self):
        """Check update and create content works"""
        c = Client()
        response = c.post('/cms/prueba', {'value': 'valordetest'})
        self.assertEqual(response.status_code, 200)
        response = c.get('/cms/prueba')
        self.assertEqual(response.status_code, 200)

    def test_update_content_put(self):
        """Check update content works with put"""
        c = Client()
        response = c.put('/cms/prueba', {'value': 'valordetestput'})
        self.assertEqual(response.status_code, 200)
        response = c.get('/cms/prueba')
        self.assertEqual(response.status_code, 200)

    def test_other_methods(self):
        """Check other methods are not allowed"""
        c = Client()
        response = c.delete('/cms/prueba')
        self.assertEqual(response.status_code, 405)
        response = c.patch('/cms/prueba')
        self.assertEqual(response.status_code, 405)
        response = c.options('/cms/prueba')
        self.assertEqual(response.status_code, 405)
        response = c.head('/cms/prueba')
        self.assertEqual(response.status_code, 405)


