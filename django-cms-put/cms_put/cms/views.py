from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseNotAllowed
from .models import Content, Coment
import datetime
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
# Para usar put y post en django necesitamos decoradores de datos


@csrf_exempt
def get_content(request, key):
    keys = Content.objects.values_list('key', flat=True)
    if request.method == "PUT":
        value = request.body.decode('utf-8')
        update_content(request, key, keys, value)
        keys = Content.objects.values_list('key', flat=True)
    elif request.method == "POST":
        if "value" in request.POST:
            value = request.POST.get('value')
            update_content(request, key, keys, value)
            keys = Content.objects.values_list('key', flat=True)
        if "title" and "body" in request.POST:
            title = request.POST.get('title')
            body = request.POST.get('body')
            com_db = Coment(title=title, body=body,
                            content=Content.objects.get(key=key),
                            timestamp=datetime.datetime.now())
            com_db.save()
    elif request.method != "GET":
        return HttpResponseNotAllowed(['GET', 'PUT', 'POST'])
    if key in keys:
        content = Content.objects.get(key=key)
        template = loader.get_template('content.html')
        context = {
            'content': content,
            'coments': content.coment_set.all()
        }
        return HttpResponse(template.render(context, request))
    else:
        raise Http404("Resource not found for key: " + key)


def update_content(request, key, keys, value):
    if key in keys:
        content = Content.objects.get(key=key)
        content.value = value
    else:
        content = Content(key=key, value=value)
    content.save()


def index(request):
    contents = Content.objects.all()
    # cargo la template que voy a usar
    template = loader.get_template('index.html')
    # preparo los datos que voy a pasar a la template
    context = {
        'contents': contents
    }
    # renderizo la template con los datos
    return HttpResponse(template.render(context, request))


# lo siguiente no tiene que ver con las vistas, sino con tests
class Counter():
    def __init__(self):
        self.count = 0

    def increment(self):
        self.count += 1
        return self.count
