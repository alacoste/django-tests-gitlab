
# Create your models here.
from django.db import models

# Create your models here. Definición del esquema de la base de datos
class Content(models.Model):
    key = models.CharField(max_length=64)
    value = models.TextField()

    def __str__(self):
        return self.key

class Coment(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField(blank=False)
    timestamp = models.DateTimeField('publicado')
    content = models.ForeignKey(Content, on_delete=models.CASCADE)

    def __str__(self):
        return self.title + " - " + self.content.__str__()