# Django Tests en GitLab

Repositorio de plantilla para el ejercicio "Django Tests en GitLab". Recuerda que puedes consultar su enunciado en la guía de estudio (programa) de la asignatura.

He añadido 2 tests adicionales a los que vimos en clase. 

- Un test que comprueba que usando el metodo PUT el servidor funciona correctamente
- Otro test que comprueba que usando otro método que no sea POST, PUT o GET el servidor responda con un código 405